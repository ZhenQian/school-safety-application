//

//  ReportViewController.swift

//  School Safety App

//

//  Created by Zachary Timberlake on 2/14/18.

//  Copyright © 2018 Zhen Qian. All rights reserved.

//



import UIKit



class ReportViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    @IBOutlet weak var issueTitle: UITextField!
    
    @IBOutlet weak var issueDescription: UITextField!
    
    var issueCoordinateX: Double = 0.0
    
    var issueCoordinateY: Double = 0.0
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view.
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
        
    }
    
    @IBAction func Add_Picture(_ sender: Any) {
        
        
        let myalert = UIAlertController(title:"Select", message:"", preferredStyle: .actionSheet)
        let cameraActioni = UIAlertAction(title:"camera",style: .default){(action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagepicker = UIImagePickerController()
                imagepicker.delegate = self
                imagepicker.sourceType=UIImagePickerControllerSourceType.camera
                
                imagepicker.allowsEditing = false
                self.present(imagepicker,animated: true,completion: nil)
            }
        }
        let cameraRollActioni = UIAlertAction(title:"camera Roll",style: .default){(action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            {
                let imagepicker = UIImagePickerController()
                imagepicker.delegate = self
                imagepicker.sourceType=UIImagePickerControllerSourceType.photoLibrary
                
                imagepicker.allowsEditing = false
                self.present(imagepicker,animated: true,completion: nil)
            }
            
        }
        myalert.addAction(cameraActioni)
        myalert.addAction(cameraRollActioni)
        self.present(myalert,animated: true)
        
    }
    
    @IBAction func sendReport(_ sender: Any) {
        
    }
    
    
    
    /*
     
     // MARK: - Navigation
     
     
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
     // Get the new view controller using segue.destinationViewController.
     
     // Pass the selected object to the new view controller.
     
     }
     
     */
    
    
    
}
