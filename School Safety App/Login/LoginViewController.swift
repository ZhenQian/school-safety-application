//
//  LoginViewController.swift
//  School Safety App
//
//  Created by Yuehao Hua on 2/15/18.
//  Copyright © 2018 Zhen Qian. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {
    @IBOutlet weak var usernameT: UITextField!
    @IBOutlet weak var passwordT: UITextField!
    @IBOutlet weak var resultL: UILabel!

    var afManager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string :"http://13.57.26.66/safety/list_reports")!
        afManager.request(url, method: .get)
            .responseJSON { response in

                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)") // original server data as UTF8 string
                    
                }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func logS(_ sender: UIButton){
        let userValue = usernameT.text!
        let passValue = passwordT.text!
        
        if userValue.isEmpty == true || passValue.isEmpty == true
        {
            resultL.text = "Invalid Username or Password!"
            return
        }
        
        let loginJSON  = [
            "username" : userValue,
            "password" : passValue,
        ]
        let url = URL(string: "http://13.57.26.66/safety/login")!
        print(loginJSON)
        afManager.request(url, method: .post, parameters: loginJSON, encoding: JSONEncoding.prettyPrinted)
            .responseJSON { response in
                //print("Request: \(String(describing: response.request))")   // original url request
                //print("Response: \(String(describing: response.response))") // http url response
                //print("Result: \(response.result)")                         // response serialization
                 debugPrint(response)
                response.result.ifSuccess {
                    self.resultL.text = "Processing..."
                    if let json = response.result.value as? [String: Any]{
                        //print("JSON: \(json)") // serialized json response
                        print(json)
                        var userTypeT = json["UserType"] as! String
                        var userType = Int(userTypeT)
                        print(userType)
                        if userType == 0 {
                            self.performSegue(withIdentifier: "logst", sender: self)
                        }
                        else if userType == 1{
                            self.performSegue(withIdentifier: "logfa", sender: self)
                        }
                    }
                    
                }
                response.result.ifFailure {
                    self.resultL.text = "Invalid Username/Password!"
                }
                
        }
        
        

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logfa"{
            if let dest = segue.destination as? mainPageFViewController{
                dest.afManger = self.afManager
            }
        }
        else if segue.identifier == "logst"{
            if let dest = segue.destination as? mainPageViewController{
                dest.afManager = self.afManager
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
