//
//  ResetViewController.swift
//  School Safety App
//
//  Created by Yuehao Hua on 3/28/18.
//  Copyright © 2018 Zhen Qian. All rights reserved.
//

import UIKit
import Alamofire

class ResetViewController: UIViewController {

    
    @IBOutlet weak var userNameT: UITextField!
    @IBOutlet weak var oldPassT: UITextField!
    @IBOutlet weak var newPassT: UITextField!
    @IBOutlet weak var conPassT: UITextField!
    @IBOutlet weak var resultL: UILabel!
    var afManager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string :"http://13.57.26.66/safety/")!
        afManager.request(url, method: .get)
            .responseJSON { response in
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    //print("Data: \(utf8Text)") // original server data as UTF8 string
                }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetPass(_ sender: UIButton) {
        print("item")
        let userValue = userNameT.text!
        let passValue = oldPassT.text!
        let newPass = newPassT.text!
        let conPass = conPassT.text!
        var flag:Bool = false
        if userValue.isEmpty || passValue.isEmpty || newPass.isEmpty || conPass.isEmpty{
            resultL.text = "Invalid Username or Password!"
            return
        }
        
        let loginJSON = [
            "username" : userValue,
            "password" : passValue,
        ]
        
        let url = URL(string: "http://13.57.26.66/safety/login")!
        afManager.request(url, method: .post, parameters: loginJSON, encoding: JSONEncoding.prettyPrinted)
            .responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                //print("Response: \(String(describing: response.response))") // http url response
                //print("Result: \(response.result)")                         // response serialization
                response.result.ifSuccess {
                    if newPass == conPass{
                        print(true)
                        let resetJSON = [
                            "password" : newPass,
                        ]
                        
                        let url2 = URL(string: "http://13.57.26.66/safety/change_password")!
                        
                        self.afManager.request(url2, method: .post, parameters: resetJSON, encoding: JSONEncoding.prettyPrinted).response{ response in
                            //print("Request: \(String(describing: response.request))")   // original url request
                            //print("Response: \(String(describing: response.response))") // http url response
                            //print("Result: \(response.result)")                         // response serialization
                            //print(response.response?.statusCode)
                            if response.response?.statusCode == 200{
                                self.resultL.text = "Password Reset"
                            }
                        }
                    }
                    else{
                        self.resultL.text = "New Password not Match!"
                    }
                }
                response.result.ifFailure {
                    self.resultL.text = "Invalid Username/Password!"
                }
                
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
