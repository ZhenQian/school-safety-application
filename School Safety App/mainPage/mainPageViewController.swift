//
//  mainPageViewController.swift
//  School Safety App
//
//  Created by Yuehao Hua on 2/15/18.
//  Copyright © 2018 Zhen Qian. All rights reserved.
//

import UIKit
import Alamofire

class mainPageViewController: UIViewController {
    
    var afManager:SessionManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goNurse(_ sender: Any) {
        
        performSegue(withIdentifier: "gonurse", sender: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gonurse"{
            if let dest = segue.destination as? NurseViewController{
                dest.afManger = self.afManager
            }
        }
        
    }


}
