//
//  StatusTableViewCell.swift
//  School Safety App
//
//  Created by 李浩楠 on 2018/3/1.
//  Copyright © 2018年 Zhen Qian. All rights reserved.
//

import UIKit

class StatusTableViewCell: UITableViewCell {

    @IBOutlet weak var STitle: UILabel!
    @IBOutlet weak var SType: UILabel!
    @IBOutlet weak var SStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
