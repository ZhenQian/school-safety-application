//
//  ViewStatusController.swift
//  School Safety App
//
//  Created by 李浩楠 on 2018/3/1.
//  Copyright © 2018年 Zhen Qian. All rights reserved.
//

import UIKit

class ViewStatusController: UIViewController,UITableViewDelegate, UITableViewDataSource{
    
    
 
    

    let elements = ["report1", "report2", "report3"]
    let types = ["report","nurse","counselor"]
    let report_st = ["Red_Light","Red_Light","Red_Light"]
    @IBOutlet weak var STableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        STableView.delegate = self;
        STableView.dataSource = self;

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements.count;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! StatusTableViewCell
        cell.STitle.text = elements[indexPath.row]
        cell.SType.text = types[indexPath.row]
        cell.SStatus.image = UIImage(named: report_st[indexPath.row])
        
        return cell;
    }
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
