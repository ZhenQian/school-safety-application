//
//  ReportTableViewCell.swift
//  School Safety App
//
//  Created by 李浩楠 on 2018/2/14.
//  Copyright © 2018年 Zhen Qian. All rights reserved.
//

import UIKit

class ReportTableViewCell: UITableViewCell {

  
    @IBOutlet weak var reportname: UILabel!
    
    @IBOutlet weak var reporttype: UILabel!
    
    @IBOutlet weak var reportstatus: UIImageView!
    
    @IBOutlet weak var icons: UIImageView!
    
    
    @IBOutlet weak var reporttime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
