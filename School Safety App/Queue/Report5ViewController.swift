//
//  Report5ViewController.swift
//  School Safety App
//
//  Created by 李浩楠 on 2018/2/14.
//  Copyright © 2018年 Zhen Qian. All rights reserved.
//

import UIKit

class Report5ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    //might use the coredata in future build, in order to store vars...
    
    
    // use to store report name
    var elements = ["report1", "report2", "report3"]
    // use to store report type
    var types = ["report","nurse","counselor"]
    // colors
    var st = ["Red_Light","Yellow_Light","Green_Light"]
    // use to sotre report color
    var report_st = ["Red_Light","Red_Light","Red_Light"]
    // icon should be same with report type
    var ic = ["report_icon","nurse_icon","counselor_icon"]
    //use to sotre name of reporter
    var s_name = ["jack","lucy","mary"]
    //use to store issue
    var elem_m = ["report fight","meet nurse","meet counselor"]
    //use to store descrption
    var descrip = ["fight","appointment","talk" ]
    //use to store time of reported
    var times = ["2018 - 2 - 18","2018 - 3 - 5", "2018 - 3 - 7"]
    
    
    
    var myIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.reloadData()
    NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        tableview.delegate = self;
        tableview.dataSource = self;
        
    // Do any additional setup after loading the view.
    }
    @objc func loadList(notification: NSNotification){
        //load data here
        self.tableview.reloadData()
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var tableview: UITableView!
    
    
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! ReportTableViewCell
        cell.reportname.text = elements[indexPath.row]
        cell.reporttype.text = types[indexPath.row]
        cell.reportstatus.image = UIImage(named: report_st[indexPath.row])
        cell.icons.image = UIImage(named: ic[indexPath.row])
        cell.reporttime.text = times[indexPath.row]
        return cell;
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myIndex = indexPath.row
        //let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! ReportTableViewCell
        let cell = tableView.cellForRow(at: indexPath) as! ReportTableViewCell
        cell.reportstatus.image = UIImage(named: st[1])
        //upon click cell, updata Red_Light to Yellow_Light to server
        report_st[myIndex] = "Yellow_Light"
        print(report_st)
        performSegue(withIdentifier: "segue", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? report6ViewController {
            destination.st_name = s_name[(tableview.indexPathForSelectedRow?.row)!]
            destination.issue = elem_m[(tableview.indexPathForSelectedRow?.row)!]
            destination.descri = descrip[(tableview.indexPathForSelectedRow?.row)!]
        }
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        myIndex = indexPath.row
        
        let delete = UITableViewRowAction(style: .destructive, title: "delete") { (action, indexPath) in
            SweetAlert().showAlert("Are you sure?", subTitle: "This report will be deleted", style: AlertStyle.warning, buttonTitle:"No", buttonColor:UIColor.colorFromRGB(0xD0D0D0) , otherButtonTitle:  "Yes", otherButtonColor: UIColor.colorFromRGB(0xDD6B55)) { (isOtherButton) -> Void in
                if isOtherButton == true {
                    SweetAlert().showAlert("Cancelled!", subTitle: "", style: AlertStyle.error)
                }
                else {
                    // delete item at indexPath
                    self.elements.remove(at: self.myIndex)
                    self.types.remove(at: self.myIndex)
                    self.report_st.remove(at: self.myIndex)
                    self.ic.remove(at: self.myIndex)
                    self.s_name.remove(at:self.myIndex)
                    self.elem_m.remove(at: self.myIndex)
                    self.descrip.remove(at: self.myIndex)
                    self.times.remove(at: self.myIndex)
                    print(self.elements)
                    tableView.reloadData()
                }
            }
        }
       return [delete]
    }
    /*
    // MARK: - Navigation
     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
