//
//  report6ViewController.swift
//  School Safety App
//
//  Created by 李浩楠 on 2018/2/14.
//  Copyright © 2018年 Zhen Qian. All rights reserved.
//

import UIKit

class report6ViewController: UIViewController {

    @IBOutlet weak var student_name: UITextField!
    @IBOutlet weak var issues: UITextField!
    
    @IBOutlet weak var descr: UITextView!
    var st_name = ""
    var issue = ""
    var descri = ""
    
 
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        student_name.text = st_name
        issues.text = issue
        descr.text = descri
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func done_reports(_ sender: Any) {
        SweetAlert().showAlert("Are you sure?", subTitle: "This report will marked as done ", style: AlertStyle.warning, buttonTitle:"No", buttonColor:UIColor.colorFromRGB(0xD0D0D0) , otherButtonTitle:  "Yes", otherButtonColor: UIColor.colorFromRGB(0xDD6B55)) { (isOtherButton) -> Void in
            if isOtherButton == true {
                
                SweetAlert().showAlert("Cancelled!", subTitle: "", style: AlertStyle.error)
            }
            else {
                
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
