//
//  CameraViewController.swift
//  School Safety App
//
//  Created by Zhen Qian on 3/15/18.
//  Copyright © 2018 Zhen Qian. All rights reserved.
//

import UIKit
import AVFoundation
class CameraViewController: UIViewController {
   var captureSession = AVCaptureSession()
    var backCamera:AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCmaera:AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureSesion()
        setupDevice()
        setupInputDevice()
        setUppreviewLayer()
        sartRunningCapturesession()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupCaptureSesion()
    {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        
    }
    func setupDevice()
    {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        for device in devices
        {
            if device.position == AVCaptureDevice.Position.back{
                backCamera =  device}
        
        else if device.position == AVCaptureDevice.Position.front{
        
                frontCamera = device}
            
        }
        currentCmaera = backCamera
    }
    func setupInputDevice()
    {
        do{
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCmaera!)
            captureSession.addInput(captureDeviceInput)
          //  photoOutput = AVCapturePhotoOutput()
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format:[AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil
            )
           // captureSession.addOutput(photoOutput!)
            
            
            
        }
        catch{
            print(error)
        }
        
    }
    func setUppreviewLayer()
    {cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        cameraPreviewLayer?.frame = self.view.frame
        self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
        
    }
    func sartRunningCapturesession()
    {
        captureSession.startRunning()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBOutlet weak var camera_button: UIButton!
    
    @IBAction func camera_button(_ sender: Any) {
       // photoOutput?.capturePhoto(with:settings,delegate:self)
        //self.performSegue(withIdentifier: "show_photo_segue", sender: nil)
    }
}
/*extension CameraViewController:AVCapturePhotoCaptureDelegate
{
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let imageData = photo.fileDataRepresentation(){
            print(imageData)
        }
    }
}*/
