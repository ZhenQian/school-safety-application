//
//  NurseViewController.swift
//  School Safety App
//
//  Created by Zhen Qian on 2/15/18.
//  Copyright © 2018 Zhen Qian. All rights reserved.
//

import UIKit
import Alamofire
class NurseViewController: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var reportTitle: UITextView!
    @IBOutlet weak var reportDetail: UITextView!
   
    var afManger:SessionManager?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func Back(_ sender: Any) {
         self.performSegue(withIdentifier: "Nurse", sender: self)
    }
    @IBAction func Add_Picture(_ sender: Any) {
        
        
        let myalert = UIAlertController(title:"Select", message:"", preferredStyle: .actionSheet)
        let cameraActioni = UIAlertAction(title:"camera",style: .default){(action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagepicker = UIImagePickerController()
                imagepicker.delegate = self
                imagepicker.sourceType=UIImagePickerControllerSourceType.camera
                
                imagepicker.allowsEditing = false
                self.present(imagepicker,animated: true,completion: nil)
            }
        }
        let cameraRollActioni = UIAlertAction(title:"camera Roll",style: .default){(action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            {
                let imagepicker = UIImagePickerController()
                imagepicker.delegate = self
                imagepicker.sourceType=UIImagePickerControllerSourceType.photoLibrary
                
                imagepicker.allowsEditing = false
                self.present(imagepicker,animated: true,completion: nil)
            }
            
        }
        myalert.addAction(cameraActioni)
        myalert.addAction(cameraRollActioni)
        self.present(myalert,animated: true)

    }
    
    @IBAction func reportButton(_ sender: Any) {
        SweetAlert().showAlert("Are you sure?", subTitle: "You report will be posted", style: AlertStyle.warning, buttonTitle:"No", buttonColor:UIColor.colorFromRGB(0xD0D0D0) , otherButtonTitle:  "Yes", otherButtonColor: UIColor.colorFromRGB(0xDD6B55)) { (isOtherButton) -> Void in
            if isOtherButton == true {
                
                SweetAlert().showAlert("Cancelled!", subTitle: "", style: AlertStyle.error)
            }
            else {
                SweetAlert().showAlert("Deleted!", subTitle: "Your imaginary file has been deleted!", style: AlertStyle.success)
               
                let url = URL(string: "http://13.57.26.66/safety/submit_report")
                var reportType = 1
                var xcood=1
                var ycood=1
               
                let image = self.imageview.image!
                let parameters = [
                    
                    "title" : self.reportTitle.text!,
                    "body" : self.reportDetail.text!,
                    "reportType": 0,
                    "xcoord": 1,
                    "ycoord": ycood,
                    
                    
                    
                    ] as [String : Any]
                self.afManger?.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted)
                    .response { response in
                        //print("Request: \(String(describing: response.request))")   // original url request
                        //print("Response: \(String(describing: response.response))") // http url response
                        //print("Result: \(response.result)")
                        // response serialization
                      print("ss")
                        debugPrint(response)
                       
                        
                }
                
                
            /*
                Alamofire.upload(multipartFormData: { multipartFormData in
                    if let imageData = UIImagePNGRepresentation(image) {
                        multipartFormData.append(imageData, withName: "picture", fileName: "Picture1.jpg", mimeType: "image/png")
                    }
                    for (key, value) in parameters {
                        if value is String || value is Int {
                            multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                        }}
                   
                  }, to: "http://13.57.26.66/safety/submit_report", method: .post, headers: nil,
                        encodingCompletion: { encodingResult in
                             // debugPrint(encodingResult)
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.response { [weak self] response in
                                    guard let strongSelf = self else {
                                        return
                                    }
                                    //print(response.data)
                                //    print(strongSelf)
                                    
                                 debugPrint(response)
                                    
                                }
                            case .failure(let encodingError):
                                print("error:\(encodingError)")
                            }
                })*/
                
                
                
                
               // self.performSegue(withIdentifier: "Nurse", sender: self)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
